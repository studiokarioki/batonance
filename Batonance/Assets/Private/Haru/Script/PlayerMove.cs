using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
	[SerializeField]
	private Animator animator;
	private CharacterController characterController;
	//　速度
	private Vector3 velocity;
	//　ジャンプ力
	[SerializeField]
	private float jumpPower = 500f;

	void Start()
	{
		characterController = GetComponent<CharacterController>();
		velocity = Vector3.zero;
	}

	void Update()
	{
		if (characterController.isGrounded)
		{
			velocity = Vector3.zero;

			//　ジャンプキー（デフォルトではSpace）を押したらY軸方向の速度にジャンプ力を足す
			if (Input.GetKeyDown(KeyCode.Space))
			{
				animator.SetTrigger("Jump");
				velocity.y += jumpPower;
			}
		}

		velocity.y += Physics.gravity.y * Time.deltaTime;
		characterController.Move(velocity * Time.deltaTime);
	}
}
