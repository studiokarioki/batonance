using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class EnemyHP : MonoBehaviour, IDamagable
{
    private float maxHp;
    public float hp;
    [SerializeField]
    private TextMeshProUGUI endUIText;
    [SerializeField, Tooltip("EnemyのHPバー")]
    private Slider slider;
    private Camera cameraObject;
    private bool openHpBer;
    public void Damage(float damage)
    {
        hp -= damage;
        slider.value = (float)hp / (float)maxHp;
        if(!openHpBer)
        {
            openHpBer = true;
            transform.parent.gameObject.GetComponent<Canvas>().enabled = true;
        }
        if (hp<=0)
        {
            Debug.Log("ゲームクリア");
            endUIText.text = "Game Clear";
            Time.timeScale = 0;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        maxHp = hp;
        cameraObject = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = cameraObject.transform.rotation;
    }
}
