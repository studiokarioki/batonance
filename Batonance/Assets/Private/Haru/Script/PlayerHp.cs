using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerHP : MonoBehaviour, IDamagable
{
    private float maxHp;
    public float hp;
    [SerializeField]
    private Text endUIText;
    private Slider slider;
    public void Damage(float damage)
    {
        if (gameObject.tag == "Just") {
            RhythmPartController.instance.StartCoroutine("Rhythm");
            return;
        }
        hp -= damage;
        slider.value = (float)hp / (float)maxHp;
        if (hp<=0)
        {
            Debug.Log("ゲームオーバー");
            //endUIText.text = "Game Over";
        }
    }
    // Start is called before the first frame update
    void Awake()
    {
        slider = GetComponent<Slider>();
        maxHp = hp;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
