using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageSuffered: MonoBehaviour
{
    [SerializeField]
    private GameObject hpBarObj;
    private IDamagable hpBar;
    private void OnTriggerEnter(Collider other)
    {
        if(this.gameObject.CompareTag("Player"))
        {
            if (other.gameObject.CompareTag("EnemyAttack"))
            {
                DamageManager.instance.DamageCalculation(other.GetComponent<NomalAttack>().attackPower, hpBar);
            }
        }
        else
        {
            if (other.gameObject.CompareTag("Attack"))
            {
                DamageManager.instance.DamageCalculation(other.GetComponent<NomalAttack>().attackPower, hpBar);
            }
        }
        
    }
    // Start is called before the first frame update
    void Start()
    {
        hpBar = hpBarObj.GetComponent<IDamagable>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
