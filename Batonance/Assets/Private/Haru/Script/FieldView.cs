using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FieldView : MonoBehaviour
{
    private EnemyMove moveScript;
    // Start is called before the first frame update
    void Start()
    {
        moveScript = transform.root.gameObject.GetComponent<EnemyMove>();
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other);
        if(other.gameObject==moveScript.player)//プレイヤーが視野範囲に入ってきた時
        {
            moveScript.isLooking = true;
            moveScript.navmesh.isStopped = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == moveScript.player)//プレイヤーが視野範囲からでて行った時
        {
            moveScript.isLooking = false;
            moveScript.navmesh.isStopped = true;
        }
    }
}
