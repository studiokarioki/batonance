using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RhythmPartController : MonoBehaviour
{
    [SerializeField, Tooltip("リズムを入れる")]
    private List<string> rhythmList = new List<string>();
    [SerializeField, Tooltip("何拍子あるか")]
    private int beatCount = 8;
    [SerializeField, Tooltip("一拍の間隔")]
    private float interval = 0.25f;
    [SerializeField, Tooltip("譜面")]
    private string score;
    [SerializeField, Tooltip("ノーツ")]
    private List<CanvasGroup> notes = new List<CanvasGroup>();
    [SerializeField, Tooltip("CreateRhythmRingの参照")]
    private List<CreateRhythmRing> createRhythmRing = new List<CreateRhythmRing>();

    // インスタンス
    public static RhythmPartController instance;

    // ノーツの数
    private int notesCount;
    // クリックの数
    private int clickCount = 0;
    // クリックする対象になるリング
    private List<CreateRhythmRing> clickRingList = new List<CreateRhythmRing>();
    [SerializeField, Tooltip("クリックする対象になるリングのサイズ")]
    private List<Transform> ringSizeList = new List<Transform>();
    private List<Transform> judgeRingSizeList = new List<Transform>();

    [SerializeField]
    private AudioClip notesSound;
    private AudioSource audioSource;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            // DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        // ノーツを消す
        for (int i = 0; i < notes.Count; i++) {
            notes[i].alpha = 0;
        }
        //Componentを取得
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // 通常状態ならリターン
        if (StateManager.instance.isNormalMode) return;

        if (Input.GetKeyDown(KeyCode.Return)) StartCoroutine(Rhythm());
        if (Input.GetMouseButtonDown(0)) PlayPart();
    }

    public IEnumerator Rhythm()
    {
        // リズムをランダムで決定
        int randomRhythm = Random.Range(0, rhythmList.Count);

        // 譜面を決定
        score = rhythmList[randomRhythm];
        Debug.Log(score);

        // ノーツの数を数える
        notesCount = score.Length - score.Replace('1'.ToString(), "").Length;

        // 一拍の時間
        var waitTime = new WaitForSeconds(interval);

        // 4/4拍子
        for (int i = 0; i < 4; i++) {
            audioSource.PlayOneShot(notesSound);
            yield return waitTime;
            yield return waitTime;
        }

        // ノーツを表示
        for (int i = 0; i < beatCount*2; i++) {
            if (i < beatCount) {
                if (score[i] == '1') {
                    createRhythmRing[i].isDuringRhythmAttack = true;
                    audioSource.PlayOneShot(notesSound);
                    createRhythmRing[i].DisplayRing();
                    clickRingList.Add(createRhythmRing[i]);
                    judgeRingSizeList.Add(ringSizeList[i]);
                    notes[i].alpha = 1;
                }
                yield return waitTime;
            }
            else {
                // createRhythmRing[i-beatCount].ResizeNotes();
                yield return waitTime;
            }
        }

        // ノーツを消す
        for (int i = 0; i < notes.Count; i++) {
            createRhythmRing[i].InitializedLineRenderer();
            notes[i].alpha = 0;
        }

        // 通常状態に戻る
        StateManager.instance.ChangeState();
        clickCount = 0;
        clickRingList.Clear();
        judgeRingSizeList.Clear();
        Debug.Log($"リズムパート終了");
    }

    public void PlayPart()
    {
        // リングの縮小を止める
        clickRingList[clickCount].canStopResize = false;

        // 判定
        JudgeRhythm(clickCount);

        // クリック回数をカウント
        clickCount++;
    }

    [SerializeField, Tooltip("ジャストの大きさ")]
    private float justSize = 2.0f;
    [SerializeField, Tooltip("パーフェクトになる誤差範囲")]
    private float perfectRange = 0.2f;
    [SerializeField, Tooltip("グレートになる誤差範囲")]
    private float goodRange = 0.5f;
    [SerializeField, Tooltip("必殺技の固定値")]
    private int fixedDamage = 5;
    [SerializeField, Tooltip("パーフェクト時のダメージボーナス")]
    private int perfectBonus = 5;
    [SerializeField, Tooltip("グレート時のダメージボーナス")]
    private int goodBonus = 3;
    [SerializeField, Tooltip("パーフェクトSE")]
    public AudioClip perfectSound;
    [SerializeField, Tooltip("グッドSE")]
    public AudioClip goodSound;
    [SerializeField, Tooltip("ミスSE")]
    public AudioClip missSound;

    private float playerJudge;
    private int totalDamage;

    private void JudgeRhythm(int judgeTarget)
    {
        // プレイヤーの判定
        playerJudge = Mathf.Abs(justSize - judgeRingSizeList[judgeTarget].localScale.x);

        if (playerJudge <= perfectRange) {
            // パーフェクトのSE
            audioSource.PlayOneShot(perfectSound);
            totalDamage += perfectBonus;
            Debug.Log($"パーフェクト");
        }
        else if (playerJudge <= goodRange) {
            // グッドのSE
            audioSource.PlayOneShot(goodSound);
            totalDamage += goodBonus;
            Debug.Log($"グッド");
        }
        else {
            // ミスのSE
            audioSource.PlayOneShot(missSound);
            Debug.Log($"ミス");
        }
        // Debug.Log("ボーナス：" + totalDamage);
        // Debug.Log($"回数" + judgeTarget);
        // Debug.Log($"対象" + (judgeRingSizeList.Count - 1));
        
        // 最後までお預け
        if (judgeTarget != judgeRingSizeList.Count - 1) return;
        totalDamage += fixedDamage;
        Debug.Log($"ドーン：" + totalDamage);

        // ダメージ算出
        DamageManager.instance.SpecialMoveDamage(totalDamage);
        // 合計ダメージの初期化
        totalDamage = 0;
        Debug.Log($"初期化：" + totalDamage);
    }
}
