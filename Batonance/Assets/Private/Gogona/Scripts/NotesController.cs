using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NotesController : MonoBehaviour
{
    [Tooltip("ノーツがサイズを変えるかどうか")]
    public bool isDuringRhythmAttack = false;
    [SerializeField, Tooltip("元のサイズ")]
    private Vector3 baseSize = new Vector3(5.0f, 5.0f, 5.0f);
    [SerializeField, Tooltip("小さくなる速度")]
    private float speedGettingSmall = 2.0f;
    [SerializeField, Tooltip("CanvasGroupの参照")]
    private CanvasGroup canvasGroup;
    // 代入用変数
    private Vector3 scale;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // リズムアタック中じゃなければリターン
        if (!isDuringRhythmAttack) return;

        if (transform.localScale.x <= 0) return;

        // 一定スピードで小さくなる
        scale = new Vector3(speedGettingSmall, speedGettingSmall, speedGettingSmall);
        transform.localScale -= scale * Time.deltaTime;
    }

    public void ResizeNotes()
    {
        // 元のサイズに戻す
        isDuringRhythmAttack = false;
        // canvasGroup.alpha = 0;
        transform.localScale = baseSize;
    }
}
