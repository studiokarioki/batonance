using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{
    [Tooltip("通常状態かどうか")]
    public bool isNormalMode = true;
    public static StateManager instance;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    private void Awake()
    {
        if (instance == null) {
            instance = this;
        }
        else Destroy(gameObject);
    }

    /// <summary>
    /// ステートの変更
    /// </summary>
    public void ChangeState()
    {
        isNormalMode = !isNormalMode;
    }
}
