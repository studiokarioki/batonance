using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateRhythmRing : RhythmPartController
{
    [SerializeField, Tooltip("円を描画する")]
    private LineRenderer lineRenderer;
    [SerializeField, Tooltip("半径")]
    private float radius = 5;
    [SerializeField, Tooltip("線の太さ")]
    private float lineWidth = 5;
    [Tooltip("ノーツがサイズを変えるかどうか")]
    public bool isDuringRhythmAttack = false;
    
    [SerializeField, Tooltip("ベースサイズ")]
    private Vector3 baseSize = new Vector3(2.5f, 2.5f, 2.5f);
    [SerializeField, Tooltip("小さくなる速度")]
    private float speedGettingSmall = 2.0f;
    [SerializeField, Tooltip("消える大きさ")]
    private float disappearSize = 0.0f;

    // 代入用変数
    private Vector3 scale;

    // 小さくのを止めるやつ
    public bool canStopResize = true;

    /// <summary>
    /// Reset is called when the user hits the Reset button in the Inspector's
    /// context menu or when adding the component the first time.
    /// </summary>
    private void Reset()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    private void Awake()
    {
        InitializedLineRenderer();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // リズムアタック中じゃなければリターン
        if (!isDuringRhythmAttack) return;

        if (transform.localScale.x <= disappearSize) {
            InitializedLineRenderer();
            return;
        }

        // 止める
        if (!canStopResize) return;

        // 一定スピードで小さくなる
        gameObject.transform.localScale -= scale * Time.deltaTime;
    }

    public void DisplayRing()
    {
        lineRenderer.enabled = true;
    }

    /// <summary>
    /// 円の初期化
    /// </summary>
    public void InitializedLineRenderer()
    {
        isDuringRhythmAttack = false;
        
        var segments = 360;
        lineRenderer.startWidth = lineWidth;
        lineRenderer.endWidth = lineWidth;
        lineRenderer.positionCount = segments;
        lineRenderer.loop = true;
        lineRenderer.useWorldSpace = false;

        var points = new Vector3[segments];
        
        for (int i = 0; i < segments; i++) {
            var rad = Mathf.Deg2Rad * (i * 360f / segments);
            var x = Mathf.Sin(rad) * radius;
            var y = Mathf.Cos(rad) * radius;
            points[i] = new Vector3(x, y, 0);
        }

        lineRenderer.SetPositions(points);
        lineRenderer.enabled = false;
        scale = new Vector3(speedGettingSmall, speedGettingSmall, speedGettingSmall);
        transform.localScale = baseSize;
        canStopResize = true;
    }
}
