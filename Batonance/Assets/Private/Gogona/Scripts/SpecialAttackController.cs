using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cysharp.Threading.Tasks;
using System;

public class SpecialAttackController : MonoBehaviour
{
    /* [SerializeField, Tooltip("必殺攻撃力")]
    private float specialAttackPower = 2.0f; */
    [SerializeField, Tooltip("必殺攻撃の時間")]
    private float specialTime = 5;
    /* [SerializeField, Tooltip("必殺攻撃モーションの準備時間")]
    private float setupTime = 2; */

    [SerializeField, Tooltip("必殺SE")]
    private AudioClip sPattackSE;
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        //Componentを取得
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    async void Update()
    {
        // 通常状態ではないならリターン
        if (!StateManager.instance.isNormalMode) return;

        // ゲージが溜まっている時にEキーを押すと必殺攻撃を呼び出す
        if (Input.GetKeyDown(KeyCode.E) && SpecialAttackGauge.instance.gaugeQuantity > 0)
            await SpecialAttack();
    }

    /// <summary>
    /// 必殺攻撃の処理
    /// </summary>
    public async UniTask SpecialAttack()
    {
        // 通常状態ではなくなる
        StateManager.instance.ChangeState();
        Debug.Log($"必殺技発動！");
        // SE
        audioSource.PlayOneShot(sPattackSE);
        await UniTask.Delay(TimeSpan.FromSeconds(1));

        // ゲージ消費
        SpecialAttackGauge.instance.ConsumedGaugePoint();

        // 必殺アニメーション

        /* // ちょっとした合間（4/4拍子）
        for (int i = 0; i < 4; i++) {
            await UniTask.Delay(TimeSpan.FromSeconds(setupTime));
        } */

        // リズムパート突入！！
        RhythmPartController.instance.StartCoroutine("Rhythm");
        await UniTask.Delay(TimeSpan.FromSeconds(specialTime));

        /* // ダメージ算出
        DamageManager.instance.DamageCalculation(specialAttackPower); */
    }
}
