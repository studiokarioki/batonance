using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SpecialAttackGauge : MonoBehaviour
{
    [SerializeField, Tooltip("ゲージポイント")]
    private int gaugePoint = 0;
    [SerializeField, Tooltip("必殺攻撃に必要なポイント数")]
    private int requiredPoint = 25;
    [Tooltip("ゲージ量")]
    public int gaugeQuantity = 0;
    [SerializeField, Tooltip("PointGaugeを入れる")]
    private Slider slider;
    [SerializeField, Tooltip("テキストを入れる")]
    private TextMeshProUGUI text;

    public static SpecialAttackGauge instance;

    /// <summary>
    /// インスタンスの生成
    /// </summary>
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            // DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        text.text = gaugeQuantity.ToString();
    }

    /// <summary>
    /// ゲージポイントを増やす処理
    /// </summary>
    /// <param name="point"></param>
    public void AddGaugePoint(int point)
    {
        // ポイントを加算
        gaugePoint += point;

        // ポイントの繰り越し、繰り上げ処理
        if (gaugePoint >= requiredPoint) {
            gaugeQuantity++;
            text.text = gaugeQuantity.ToString();
            gaugePoint -= requiredPoint;
        }

        // ゲージバー
        slider.value = (float)gaugePoint / (float)requiredPoint;
    }

    /// <summary>
    /// ゲージ消費の処理
    /// </summary>
    public void ConsumedGaugePoint()
    {
        // ゲージが溜まっていなければリターン
        if (gaugeQuantity == 0) return;

        // ゲージ消費
        gaugeQuantity--;
        text.text = gaugeQuantity.ToString();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    private void Update()
    {
        /* if (Input.GetKeyDown("1")) AddGaugePoint(15); */
    }
}
